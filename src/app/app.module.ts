import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { LayoutModule } from '@angular/cdk/layout';
import { FlexLayoutModule } from '@angular/flex-layout';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AppMaterialModule } from './app-material/app-material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DashboardComponent } from './dashboard/dashboard.component';
import { StatisticsComponent } from './statistics/statistics.component';
import { ProfileComponent } from './profile/profile.component';
import { SettingsComponent } from './settings/settings.component';
import { SignupComponent } from './signup/signup.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { GoogleChartsModule } from 'angular-google-charts';
import { GoogleChartModule } from './google-chart/google-chart.module';
import { CallTypeFormComponent } from './call-type-form/call-type-form.component';
import { CallMessagesComponent } from './call-messages/call-messages.component';
import { environment } from 'src/environments/environment';
// Firebase Modules
import { AngularFireModule } from '@angular/fire';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFirestore, AngularFirestoreModule } from '@angular/fire/firestore';
import { CustomerListComponent } from './customer-list/customer-list.component';
import { AddCustomerComponent } from './add-customer/add-customer.component';
import { AngularFireAuthModule } from '@angular/fire/auth';


import * as firebase from "firebase";
import { UserService } from './services/user.service';
import { AuthGuard } from './services/auth-guard';
import { RegisterCustomerComponent } from './register-customer/register-customer.component';
import { WindowService } from './services/window.service';


@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    StatisticsComponent,
    ProfileComponent,
    SettingsComponent,
    SignupComponent,
    CallTypeFormComponent,
    CallMessagesComponent,
    CustomerListComponent,
    AddCustomerComponent,
    RegisterCustomerComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    LayoutModule,
    AppRoutingModule,
    AppMaterialModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    GoogleChartModule,
    GoogleChartsModule.forRoot(),
    AngularFireModule.initializeApp(environment.firebaseConfig), // Main Angular fire module;
    AngularFireStorageModule, // for storage module;
    AngularFirestoreModule.enablePersistence(), // for offline supports;
    AngularFireAuthModule

  ],
  providers: [AngularFirestore, UserService, AuthGuard, WindowService],
  bootstrap: [AppComponent]
})
export class AppModule { }
