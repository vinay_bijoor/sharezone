import { User } from './entity/user';
import { UserService } from './services/user.service';
import { Component, OnDestroy, ChangeDetectorRef, ViewChild, OnInit } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import { Router, NavigationEnd, RouteConfigLoadStart } from '@angular/router';
import { MatSidenav } from '@angular/material';
import * as firebase from 'firebase';
import { environment } from 'src/environments/environment';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

// tslint:disable-next-line:align

export class AppComponent implements OnInit, OnDestroy {
  @ViewChild('sidenav', { static: false }) sidenav: MatSidenav;

  title = 'sharezone';
  mobileQuery: MediaQueryList;
  isHome: boolean;
  hideToolBar = false;
  userLoggedIn = false;

  // tslint:disable-next-line:variable-name
  private _mobileQueryListener: () => void;

  constructor(changeDetectorRef: ChangeDetectorRef, media: MediaMatcher, private router: Router,
    private fireAuth: AngularFireAuth, private userService: UserService) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);

    if (!firebase.apps.length) {
      firebase.initializeApp(environment.firebaseConfig);
    }
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.isHome = this.router.url === '' ? true : false;
        if (this.router.url.includes('signup') || this.router.url.includes('/signup')) {
          this.hideToolBar = true;
        } else {
          this.hideToolBar = false;
        }
      }
    });
  }


  ngOnInit() {
    this.getAuthDetails();
  }

  getAuthDetails() {
    this.userService.isUserLogedInChangedObservable.subscribe((currentuser) => {
      console.log(currentuser, 'currentuser');
      this.userLoggedIn = currentuser ? true : false;
    });
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

  logOut() {
    this.userService.logout();
    this.sidenav.close();
  }

  toggleSideNav() {
    this.sidenav.close();
  }
}
