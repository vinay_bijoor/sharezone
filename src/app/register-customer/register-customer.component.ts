import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CustomerService } from '../services/customer.service';
import { WindowService } from '../services/window.service';
import * as firebase from 'firebase';

@Component({
  selector: 'app-register-customer',
  templateUrl: './register-customer.component.html',
  styleUrls: ['./register-customer.component.scss']
})
export class RegisterCustomerComponent implements OnInit {
  telegramUserId: string;
  telegramChatId: string;
  phoneNumber: string;
  loginStatus: string;
  windowRef: any;
  verificationCode: string;

  constructor(private route: ActivatedRoute, private customerService: CustomerService, private win: WindowService) { }

  ngOnInit() {
    this.windowRef = this.win.windowRef;
    this.windowRef.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container');
    this.windowRef.recaptchaVerifier.render();
    this.checkForTelegramParams();
  }

  checkForTelegramParams() {
    this.route.queryParams.subscribe(params => {
      this.telegramChatId = params['session'];
      this.telegramUserId = params['user'];
      console.log("got params", this.telegramChatId);
      this.checkIfUserRegistered();
    });
  }

  checkIfUserRegistered() {
    if (this.telegramChatId) {
      this.customerService.getCustomerOfUserByTelegramChatId(this.telegramChatId).subscribe(customers => {
        if (customers && customers.length > 0) {
          this.loginStatus = 'customer-registered'
        } else {
          this.loginStatus = 'customer-new';
        }
        console.log("got customers of chat id, ", this.loginStatus, customers);
      });
    }
  }

  onLoginClick() {
    const appVerifier = this.windowRef.recaptchaVerifier;
    const num = '+91' + this.phoneNumber;
    firebase.auth().signInWithPhoneNumber(num, appVerifier)
      .then(result => {
        this.windowRef.confirmationResult = result;
        this.loginStatus = 'otp-check';
      })
      .catch(error => console.log(error));
  }

  verfiyOtpAndLogin() {
    this.windowRef.confirmationResult
      .confirm(this.verificationCode)
      .then(result => {
        if (result) {
          console.log(result.user, ' registered successfully with chatId: ', this.telegramChatId);
          this.loginStatus = 'login-success';
          //this.userService.setUserDoc(result.user, 'employee', null, this.userName);
        }
      })
      .catch(error => {
        console.log(error.code, 'error');
        this.loginStatus = 'login-error';
        //this.snackBarService.showToaster(`Invalid Otp`);
      });
  }


}
