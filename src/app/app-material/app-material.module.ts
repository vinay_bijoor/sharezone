import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// tslint:disable-next-line:max-line-length
import { MatSidenavModule, MatIconModule, MatToolbarModule, MatListModule, MatSnackBarModule, MatCardModule, MatButtonModule, MatFormFieldModule, MatSelectModule, MatAutocompleteModule, MatInputModule, MatChipsModule, MatTableModule, MatTabsModule, MatPaginatorModule } from '@angular/material';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MatSidenavModule,
    MatIconModule,
    MatToolbarModule,
    MatListModule,
    MatCardModule,
    MatButtonModule,
    MatFormFieldModule,
    MatSelectModule,
    MatAutocompleteModule,
    MatInputModule,
    MatChipsModule,
    MatTableModule,
    MatTabsModule,
    MatSnackBarModule,
    MatPaginatorModule,
  ],
  exports: [
    MatSidenavModule,
    MatIconModule,
    MatToolbarModule,
    MatListModule,
    MatCardModule,
    MatButtonModule,
    MatFormFieldModule,
    MatSelectModule,
    MatAutocompleteModule,
    MatInputModule,
    MatChipsModule,
    MatTableModule,
    MatTabsModule,
    MatSnackBarModule,
    MatPaginatorModule
  ]
})
export class AppMaterialModule { }
