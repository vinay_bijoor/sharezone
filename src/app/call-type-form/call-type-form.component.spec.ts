import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CallTypeFormComponent } from './call-type-form.component';

describe('CallTypeFormComponent', () => {
  let component: CallTypeFormComponent;
  let fixture: ComponentFixture<CallTypeFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CallTypeFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CallTypeFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
