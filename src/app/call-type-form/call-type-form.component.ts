import { Component, OnInit, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { HomeService } from '../services/home.service';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { EventEmitter } from '@angular/core';
import { CallsService } from '../services/calls.service';
import { ShareCall } from '../entity/share-call';
import { AngularFirestore } from '@angular/fire/firestore';
import { MatSnackBar } from '@angular/material';
import { IndiciesList } from '../entity/Indicies-list';
import { StockList } from '../entity/stock-list';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-call-type-form',
  templateUrl: './call-type-form.component.html',
  styleUrls: ['./call-type-form.component.scss']
})


export class CallTypeFormComponent implements OnInit {
  @Input() type = 'index_future';
  selected: string;
  formGroup: FormGroup;
  callList: any = [];
  selectedOption: string;
  indiciesListControl = new FormControl();
  stockListControl = new FormControl();
  indiciesList: IndiciesList[];
  stockList: StockList[];
  filteredIndiciesListOptions: Observable<IndiciesList[]>;
  filteredStockListOptions: Observable<StockList[]>;
  futuresDateList = [];
  userId: string;
  constructor(private formBuilder: FormBuilder, private homeService: HomeService,
    private callsService: CallsService, private snackBar: MatSnackBar, private userService: UserService) { }

  ngOnInit() {
    this.selected = 'buy';
    // this.userId = this.userService.getUserData().id;
    this.fetchStockListData();
    this.fetchIndiciesListData();
    this.createForm();
    this.getFuturesDateList();
    if (this.type === 'index_future' || this.type === 'index_options' || this.type === 'equity') {
      if (this.formGroup.controls.index) {
        this.filteredIndiciesListOptions = this.formGroup.controls.index.valueChanges
          .pipe(
            startWith(''),
            map(value => typeof value === 'string' ? value : (value ? value.name : '')),
            map(name => name ? this._filterIndiciesList(name) : [])
          );
      }
    }

    if (this.type === 'equity_future' || this.type === 'equity_options') {
      if (this.formGroup.controls.equity) {
        this.filteredStockListOptions = this.formGroup.controls.equity.valueChanges
          .pipe(
            startWith(''),
            map(value => typeof value === 'string' ? value : (value ? value.name : '')),
            map(name => name ? this._filterStockList(name) : [])
          );
      }
    }
  }

  getFuturesDateList() {
    this.futuresDateList = [new Date(),
    new Date(), new Date()];
  }

  fetchIndiciesListData() {
    this.homeService.getIndiciesListData().subscribe((data) => {
      this.indiciesList = data;
    });
  }

  fetchStockListData() {
    this.homeService.getStockListData().subscribe((data) => {
      this.stockList = data;
    });
  }

  createForm() {
    if (this.type === 'index_future') {
      this.formGroup = this.formBuilder.group({
        type: ['', Validators.required],
        index: ['', Validators.required],
        future_date: ['', Validators.required],
        trigger_price: ['', Validators.required],
        stop_loss: ['', Validators.required],
        target: ['', Validators.required],
      });
    }
    if (this.type === 'index_options') {
      this.formGroup = this.formBuilder.group({
        type: ['', Validators.required],
        index: ['', Validators.required],
        options_type: ['', Validators.required],
        trigger_price: ['', Validators.required],
        stop_loss: ['', Validators.required],
        target: ['', Validators.required]
      });
    }
    if (this.type === 'equity_future') {
      this.formGroup = this.formBuilder.group({
        type: ['', Validators.required],
        equity: ['', Validators.required],
        future_date: ['', Validators.required],
        trigger_price: ['', Validators.required],
        stop_loss: ['', Validators.required],
        target: ['', Validators.required],
      });
    }
    if (this.type === 'equity_options') {
      this.formGroup = this.formBuilder.group({
        type: ['', Validators.required],
        equity: ['', Validators.required],
        options_type: ['', Validators.required],
        trigger_price: ['', Validators.required],
        stop_loss: ['', Validators.required],
        target: ['', Validators.required]
      });
    }
    if (this.type === 'equity') {
      this.formGroup = this.formBuilder.group({
        type: ['', Validators.required],
        equity: ['', Validators.required],
        trigger_price: ['', Validators.required],
        stop_loss: ['', Validators.required],
        target: ['', Validators.required],
      });
    }
  }


  shareCallDataBasedOnType(formData: ShareCall) {
    let targetArray;
    if (formData.target) {
      targetArray = formData.target.split(',');
    }
    const shareCall = new ShareCall();
    shareCall.userId = this.userId;
    if (this.type === 'index_future') {
      shareCall.type = formData.type;
      shareCall.index = formData.index['name'];
      shareCall.future_date = formData.future_date;
      shareCall.trigger_price = formData.trigger_price;
      shareCall.stop_loss = formData.stop_loss;
      shareCall.target = targetArray;
    }

    if (this.type === 'index_options') {
      shareCall.type = formData.type;
      shareCall.index = formData.index['name'];
      shareCall.options_type = formData.options_type;
      shareCall.trigger_price = formData.trigger_price;
      shareCall.stop_loss = formData.stop_loss;
      shareCall.target = targetArray;
    }

    if (this.type === 'equity_future') {
      shareCall.type = formData.type;
      shareCall.equity = formData.equity['name'];
      shareCall.future_date = formData.future_date;
      shareCall.trigger_price = formData.trigger_price;
      shareCall.stop_loss = formData.stop_loss;
      shareCall.target = targetArray;
    }

    if (this.type === 'equity_options') {
      shareCall.type = formData.type;
      shareCall.equity = formData.equity['name'];
      shareCall.options_type = formData.options_type;
      shareCall.trigger_price = formData.trigger_price;
      shareCall.stop_loss = formData.stop_loss;
      shareCall.target = targetArray;
    }

    if (this.type === 'equity') {
      shareCall.type = formData.type;
      shareCall.equity = formData.equity['name'];
      shareCall.trigger_price = formData.trigger_price;
      shareCall.stop_loss = formData.stop_loss;
      shareCall.target = targetArray;
    }
    shareCall.shareCallType = this.type;
    return JSON.parse(JSON.stringify(shareCall));
  }


  onSubmit(formData: ShareCall) {
    const shareCall = this.shareCallDataBasedOnType(formData);
    this.callsService.createCall(shareCall);
    this.snackBar.open(`sucessfully created a ${this.type} call`, 'Sucess', { duration: 2000 });
    this.formGroup.reset();
  }

  displayIndiciesListFn(indicieslist?: IndiciesList): string | undefined {
    return indicieslist ? indicieslist.name : undefined;
  }

  displayStockListFn(stocklist?: StockList): string | undefined {
    return stocklist ? stocklist.name : undefined;
  }

  private _filterIndiciesList(name: string): IndiciesList[] {
    const filterIndiciesListValue = name.toLowerCase();
    return this.indiciesList.filter(indicieslist => indicieslist.name.toLowerCase().indexOf(filterIndiciesListValue) !== -1);
  }

  private _filterStockList(name: string): StockList[] {
    const filterStockListValue = name.toLowerCase();
    return this.stockList.filter(stocklist => stocklist.name.toLowerCase().indexOf(filterStockListValue) !== -1);
  }
}
