import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CallMessagesComponent } from './call-messages.component';

describe('CallMessagesComponent', () => {
  let component: CallMessagesComponent;
  let fixture: ComponentFixture<CallMessagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CallMessagesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CallMessagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
