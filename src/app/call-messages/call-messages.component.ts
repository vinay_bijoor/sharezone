import { UserService } from './../services/user.service';
import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { CallsService } from '../services/calls.service';
import { DatePipe } from '@angular/common';
import { ShareCall } from '../entity/share-call';

@Component({
  selector: 'app-call-messages',
  templateUrl: './call-messages.component.html',
  styleUrls: ['./call-messages.component.scss']
})
export class CallMessagesComponent implements OnInit {
  @Input() callsList: any = [];
  currentDate = new Date();
  @Output() deleteCallO = new EventEmitter();
  userId: string;
  constructor() {
  }

  ngOnInit() {
  }

  deleteCall(call) {
    this.deleteCallO.emit(call);
  }
}