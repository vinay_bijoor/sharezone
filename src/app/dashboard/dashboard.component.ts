import { User } from './../entity/user';
import { UserService } from './../services/user.service';
import { FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit, Input } from '@angular/core';
import { MatTabChangeEvent, MatSnackBar } from '@angular/material';
import { CallsService } from '../services/calls.service';
import { ShareCall } from '../entity/share-call';
import { AngularFireAuth } from '@angular/fire/auth';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})

export class DashboardComponent implements OnInit {
  type = '';
  custom_message = new FormControl('');
  userId: string;
  callsList: any = [];
  constructor(private callsService: CallsService, private snackBar: MatSnackBar,
    private fireAuth: AngularFireAuth, private userService: UserService) {
  }

  ngOnInit() {
    this.getCurrentUserId();
  }

  getCurrentUserId() {
    if (this.userService.currentUser == null) {
      this.userService.isUserLogedInChangedObservable.subscribe((data) => {
        console.log(data.uid, 'user-data');
        if (data) {
          this.fetchCallsList(data.uid);
        }
      });
    } else {
      this.fetchCallsList(this.userService.currentUser.uid);
    }
  }

  // get all call 
  fetchCallsList(userid) {
    this.callsService.getAllCall(userid).subscribe(callList => {
      this.callsList = [];
      if (callList) {
        callList.forEach(item => {
          const a = item.payload.doc.data();
          this.callsList.push(a as ShareCall);
        });
      }
      console.log(this.callsList, 'callsList');
    });
  }

  tabChanged(tabChangeEvent: MatTabChangeEvent): void {
    console.log(tabChangeEvent);
    switch (tabChangeEvent.index) {
      case 0:
        console.log(DashboardComponent.name, 'Selected Index Future');
        break;
      case 1:
        console.log(DashboardComponent.name, 'Selected Index Options');
        break;
      case 2:
        console.log(DashboardComponent.name, 'Selected Stock Future');
        break;
      case 3:
        console.log(DashboardComponent.name, 'Selected Stock Options');
        break;
      case 4:
        console.log(DashboardComponent.name, 'Selected Equity');
        break;
    }
  }

  // delete call
  deleteCall(call: ShareCall) {
    this.callsService.deleteCall(call.id);
  }

  // create custom call
  onSubmit(custom: string, type: string) {
    const shareCall = new ShareCall();
    shareCall.custom = custom;
    shareCall.shareCallType = type;
    const convertedShareObject = JSON.parse(JSON.stringify(shareCall));
    console.log(convertedShareObject, 'shareCall-object');
    // firebase expects a object not class object so converted back to object
    this.callsService.createCall(convertedShareObject);
    this.custom_message = new FormControl('');
    this.snackBar.open(`sucessfully created a ${type} call`, 'Sucess', { duration: 2000 });

  }

}
