import { Router } from '@angular/router';
import { CustomerService } from './../services/customer.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Customer } from '../entity/user';
import { MatSnackBar } from '@angular/material';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-add-customer',
  templateUrl: './add-customer.component.html',
  styleUrls: ['./add-customer.component.scss']
})
export class AddCustomerComponent implements OnInit {
  customerForm: FormGroup;
  constructor(private formBuilder: FormBuilder, private customerService: CustomerService,
    private snackBar: MatSnackBar, private router: Router, private userService: UserService) { }

  ngOnInit() {
    this.createCustomerForm();
  }

  createCustomerForm() {
    this.customerForm = this.formBuilder.group({
      name: ['', Validators.required],
      phone_number: ['', Validators.required],
    });
  }

  createCustomer(customer: Customer) {
    if (customer) {
      this.customerService.getCustomerOfUserByPhoneNumber(customer.phone_number).subscribe((customers) => {
        if (!customers || customers.length === 0) {
          const cust = JSON.parse(JSON.stringify(customer));
          this.customerService.createCustomer(cust);
          this.snackBar.open(`sucessfully created a ${customer.name} customer`, 'Sucess', { duration: 2000 });
          this.router.navigate(['/customer-list']);
        } else {
          this.snackBar.open(`customer already exixts`, 'Warn', { duration: 2000 });
        }
      });
    }
  }





}
