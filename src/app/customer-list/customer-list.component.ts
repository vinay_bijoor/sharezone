import { CustomerService } from './../services/customer.service';
import { Router } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Customer } from '../entity/user';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.scss']
})
export class CustomerListComponent implements OnInit {
  customerList: Customer[] = [];
  displayedColumns: string[] = ['name', 'phone_number'];
  customerListDataSource;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  constructor(private router: Router, private customerService: CustomerService, private userService: UserService) { }

  ngOnInit() {
    const currentUser = this.userService.currentUser;
    this.fetchCustomerList(currentUser.uid);
  }

  addCustomer() {
    this.router.navigate(['/add-customer']);
  }
  fetchCustomerList(userId) {
    this.customerService.getAllCustomers(userId).subscribe(data => {
      this.customerList = [];
      data.forEach(item => {
        const a = item.payload.doc.data();
        this.customerList.push(a as Customer);
      });
      this.customerListDataSource = new MatTableDataSource(this.customerList);
      this.customerListDataSource.sort = this.sort;
      this.customerListDataSource.paginator = this.paginator;
      console.log(this.customerList, 'Customer');
    });
  }

  applyFilter(filterValue: string) {
    this.customerListDataSource.filter = filterValue.trim().toLowerCase();
  }


}
