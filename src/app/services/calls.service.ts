import { UserService } from './user.service';
import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { ShareCall } from '../entity/share-call';
import { User } from '../entity/user';
import * as firebase from 'firebase';
import { Customer } from '../entity/user';
import { map, take, tap, retry } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CallsService {
  shareCallCollections: AngularFirestoreCollection<ShareCall>;
  currentUser;

  constructor(private angularFirestoreDb: AngularFirestore, private userService: UserService) {
    this.shareCallCollections = this.angularFirestoreDb.collection<ShareCall>('calls');
    this.getUserData();
  }

  getUserData() {
    this.currentUser = this.userService.currentUser;
  }

  /* create call */
  createCall(shareCall: ShareCall) {
    shareCall.id = this.angularFirestoreDb.createId();
    shareCall.userId = this.currentUser.uid;
    shareCall.created_date = firebase.firestore.FieldValue.serverTimestamp();
    this.shareCallCollections.add(shareCall);
  }
  /* get all  call */


  // Fetch get AllCall
  getAllCall(userId: string): Observable<any> {
    return this.angularFirestoreDb.collection<ShareCall>('calls',
      ref => ref.where('userId', '==', userId).orderBy('created_date', 'desc')).snapshotChanges().pipe(
        map(actions => {
          return actions;
        }));
  }

  // delete Call
  deleteCall(id: string) {
    const deleteCall = this.angularFirestoreDb.collection('calls', ref => ref.where('id', '==', id));
    deleteCall.get().subscribe((quer) => {
      quer.forEach((doc) => {
        doc.ref.delete();
      });
    });
  }
}
