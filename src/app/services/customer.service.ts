import { UserService } from './user.service';
import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import * as firebase from 'firebase';
import { Customer } from '../entity/user';
import { map, take, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
@Injectable({
    providedIn: 'root'
})
export class CustomerService {
    usedId: string;
    customerCollections: AngularFirestoreCollection<Customer>;
    constructor(private angularFirestoreDb: AngularFirestore, private userService: UserService) {
        this.customerCollections = this.angularFirestoreDb.collection<Customer>('customer');
        if (this.userService.currentUser)
            this.usedId = this.userService.currentUser.uid;
    }

    /* create call */
    createCustomer(customer: Customer) {
        customer.id = this.angularFirestoreDb.createId();
        customer.userId = this.usedId;
        customer.created_date = firebase.firestore.FieldValue.serverTimestamp();
        this.customerCollections.add(customer);
    }


    /* get all  customers */
    getAllCustomers(userId: string): Observable<any> {
        return this.angularFirestoreDb.collection<Customer>('customer',
            ref => ref.where('userId', '==', userId).orderBy('created_date', 'desc')).snapshotChanges().pipe(
                map(actions => {
                    return actions;
                }));
    }

    // Fetch Single Student Object
    getCustomerOfUserByPhoneNumber(phoneNumber: string): Observable<any> {
        return this.angularFirestoreDb.collection<Customer>('customer',
            ref => ref.where('userId', '==', this.usedId).where('phone_number', '==', phoneNumber)).get()
            .pipe(map(res => res.docs.map(doc => doc.data)));
    }

    getCustomerOfUserByTelegramChatId(chatId: string): Observable<any> {
        return this.angularFirestoreDb.collection<Customer>('customer',
            ref => ref.where('telegram_chat', '==', chatId)).get()
            .pipe(map(res => res.docs.map(doc => doc.data)));
    }

}