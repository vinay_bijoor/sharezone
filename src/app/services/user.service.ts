import { Injectable } from '@angular/core';
import { User } from '../entity/user';
import { Observable, Subject } from 'rxjs';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection } from '@angular/fire/firestore';
import { take, map } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material';

@Injectable({
    providedIn: 'root'
})
export class UserService {
    currentUser;
    userCollections: AngularFirestoreCollection<User>;
    private isUserLogedInChangedSource = new Subject<any>();
    isUserLogedInChangedObservable = this.isUserLogedInChangedSource.asObservable();
    constructor(
        public fireAuth: AngularFireAuth, private angularFirestoreDb: AngularFirestore,
        private router: Router, private snackBar: MatSnackBar
    ) {
        this.userCollections = this.angularFirestoreDb.collection<User>('users');
        this.fireAuth.authState.subscribe(user => {
            if (user) {
                console.log('user-service-userdata', user);
                this.setCurrentUserData(user);
                localStorage.setItem('user', JSON.stringify(this.currentUser));
                JSON.parse(localStorage.getItem('user'));
            } else {
                localStorage.setItem('user', null);
                JSON.parse(localStorage.getItem('user'));
            }
        });
    }

    getUserData() {
        if (this.currentUser) {
            return this.currentUser;
        }
    }

    setCurrentUserData(user) {
        this.currentUser = user;
        this.isUserLogedInChangedSource.next(user);
    }

    updateUserInfo(id: string, email: string, password: string) {
        const userRef: AngularFirestoreDocument<User> = this.angularFirestoreDb.doc(`users/${id}`);
        const data: User = {
            id,
            email,
            password,
        };
        return userRef.set(data, { merge: true });
    }

    /* create user */
    createuser(user: User) {
        user.id = this.angularFirestoreDb.createId();
        this.userCollections.add(user);
    }

    //// Email/Password Auth ////
    emailSignUp(email: string, password: string) {
        return this.fireAuth.auth
            .createUserWithEmailAndPassword(email, password)
            .then(user => {
                // this.updateUserData(user); // if using firestore
            })
            .catch(error => this.handleError(error));
    }

    emailLogin(email: string, password: string) {
        return this.fireAuth.auth
            .signInWithEmailAndPassword(email, password);
    }

    // Fetch user is already exixts
    checkIfUserAlreadyExitFunction(id: string): Observable<any> {
        return this.angularFirestoreDb.collection<User>('users',
            ref => ref.where('id', '==', id)).valueChanges().pipe(
                take(1),
                map(actions => {
                    return actions[0];
                }));
    }


    checkIfUserExits(id: string): Observable<boolean> {
        return this.checkIfUserAlreadyExitFunction(id).pipe(map((customer) => {
            if (customer) {
                return true;
            } else {
                return false;
            }
        }));
    }


    // If error, console log and notify user
    private handleError(error) {
        if (error.code === 'auth/popup-blocked') {
            this.snackBar.open(
                'Your browser has disabled Popups. Please Try again',
                'CLOSE'
            );
        } else if (error.code === 'auth/popup-closed-by-user') {
            this.snackBar.open('Please reload and try again.', 'CLOSE', {
                duration: 3000
            });
        } else {
            this.snackBar.open(error.message, 'CLOSE', { duration: 3500 });
        }
        return error.message;
    }


    logout() {
        return this.fireAuth.auth.signOut().then((data) => {
            localStorage.clear();
            this.setCurrentUserData(null);
            this.router.navigate(['login']);
        });
    }

}