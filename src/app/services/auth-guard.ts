import { UserService } from './user.service';
import { switchMap, take, map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/internal/Observable';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private auth: AngularFireAuth, private router: Router, private route: ActivatedRoute) { }

    canActivate(route: ActivatedRouteSnapshot, routerState: RouterStateSnapshot): Observable<boolean> {
        return this.auth.authState.pipe(
            take(1),
            map(state => {
                if (state == null) {
                    this.router.navigate(['/login']);
                } else {
                    return true;
                }
            })
        );
    }
}