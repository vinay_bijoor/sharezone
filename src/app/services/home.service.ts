import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HomeService {
  private baseurl;

  constructor(private http: HttpClient) { }

  getCallTypeData(): Observable<any> {
    this.baseurl = 'assets/json-data/call.json';
    let url = this.baseurl;
    return this.http.get(url).pipe(tap(data => console.log()));
  }

  getStockListData(): Observable<any> {
    this.baseurl = 'assets/json-data/stocklist.json';
    let url = this.baseurl;
    return this.http.get(url).pipe(tap(data => console.log()));
  }

  getIndiciesListData(): Observable<any> {
    this.baseurl = 'assets/json-data/indicies.json';
    let url = this.baseurl;
    return this.http.get(url).pipe(tap(data => console.log()));
  }
}
