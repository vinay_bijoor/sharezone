
export class ShareCall {
    id: string;
    equity: string;
    future_date: string;
    index: string;
    options_type: string;
    stop_loss: number;
    target: any;
    trigger_price: number;
    type: string;
    created_date: any;
    custom: string;
    userId: string;
    shareCallType: string;  // 'custom','index future','index options','equity future','equity options','equity'

    constructor() {
        this.id = null;
        this.equity = null;
        this.future_date = null;
        this.index = null;
        this.options_type = null;
        this.stop_loss = null;
        this.target = null;
        this.type = null;
        this.shareCallType = null;
        this.custom = null;
        this.userId = null;
        this.trigger_price = null;
        this.created_date = null;
    }

}




