export class StockList {
    symbol: string;
    name: string;
    currency: string;
    stock_exchange_long: string;
    stock_exchange_short: string;
    timezone_name: string;
}