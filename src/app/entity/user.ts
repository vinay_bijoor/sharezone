export class User {
    id: string;
    email?: string;
    password?: string;

    constructor() {
        this.id = null;
        this.email = null;
        this.password = null;

    }
}

export class Customer {
    id: string;
    name: string;
    phone_number: string;
    userId?: string;
    created_date?: any;
}