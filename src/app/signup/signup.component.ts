import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as firebase from 'firebase';
import { WindowService } from '../services/window.service';
import { environment } from 'src/environments/environment';
import { MatSnackBar } from '@angular/material';
import { UserService } from '../services/user.service';
import { User } from '../entity/user';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],
  providers: [WindowService]
})
export class SignupComponent implements OnInit {
  showOtp = false;
  windowRef: any;
  phoneNumber: string;
  Otp: string;
  verficationCode: string;
  constructor(private router: Router, private win: WindowService,
    private snackBar: MatSnackBar, private userSerivce: UserService) {
    if (!firebase.apps.length) {
      firebase.initializeApp(environment.firebaseConfig);
    }

  }

  ngOnInit() {
    // this.windowRef = this.win.windowRef;
    // this.windowRef.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container');
    // this.windowRef.recaptchaVerifier.render();
  }



  // goToOtp() {
  //   const appVerifier = this.windowRef.recaptchaVerifier;
  //   const num = '+91' + this.phoneNumber;
  //   this.userSerivce.checkIfUserExits(this.phoneNumber).subscribe((userExixts) => {
  //     console.log(userExixts, 'userExixts');
  //     if (!userExixts) {
  //       firebase.auth().signInWithPhoneNumber(num, appVerifier)
  //         .then(result => {
  //           this.windowRef.confirmationResult = result;
  //           this.showOtp = true;
  //         })
  //         .catch(error => console.log(error));
  //     } else {
  //       this.snackBar.open(`User Already Exixts`, 'Error', { duration: 2000 });
  //     }
  //   });
  // }



  // onClickLogin() {
  //   let user: User;
  //   this.windowRef.confirmationResult
  //     .confirm(this.verficationCode)
  //     .then(result => {
  //       console.log(result.additionalUserInfo, 'res');
  //       const newUser = result.additionalUserInfo.isNewUser;
  //       this.snackBar.open(`Sucesfullt Login`, 'Sucess', { duration: 2000 });
  //       this.router.navigate(['/dashboard']);
  //       user = {
  //         id: null,
  //         name: 'ankappa',
  //         phone_number: this.phoneNumber,
  //       };
  //       console.log(user, 'users');
  //       if (!newUser) {
  //         this.userSerivce.createuser(user);
  //       }
  //     })
  //     .catch(error => {
  //       console.log(error.code, 'error');
  //       this.snackBar.open(`Invalid Otp`, 'Error', { duration: 2000 });
  //     });
  // }

  emailLogin(email: string, password: string) {
    this.userSerivce.emailLogin(email, password).then(user => {
      const userData = user.user;
      if (user) {
        this.router.navigate(['/dashboard']);
        localStorage.setItem('userId', userData.uid);
        this.userSerivce.setCurrentUserData(userData);
        this.userSerivce.checkIfUserExits(userData.uid).subscribe((data) => {
          console.log(data, 'user-exits');
          if (!data) {
            this.userSerivce.updateUserInfo(userData.uid, email, password); // if using firestore
          }
        });
      }

    })
      .catch(error => this.handleError(error));
  }


  // If error, console log and notify user
  private handleError(error) {
    if (error.code === 'auth/popup-blocked') {
      this.snackBar.open(
        'Your browser has disabled Popups. Please Try again',
        'CLOSE'
      );
    } else if (error.code === 'auth/popup-closed-by-user') {
      this.snackBar.open('Please reload and try again.', 'CLOSE', {
        duration: 3000
      });
    } else {
      this.snackBar.open(error.message, 'CLOSE', { duration: 3500 });
    }
    return error.message;
  }

}

